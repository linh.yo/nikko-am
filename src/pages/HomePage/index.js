import React from 'react'
import './index.scss'
import HomeIntro from './components/HomeIntro'
import HomeInsight from './components/HomeInsight'
import HomeFund from './components/HomeFund'
import HomeInvest from './components/HomeInvest'

function HomePage() {
  return (
    <article id="home-page">
      <HomeIntro />
      <HomeInsight />
      <HomeFund />
      <HomeInvest />
    </article>
  )
}

export default HomePage
