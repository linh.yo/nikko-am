import React, { useState } from 'react'
import { Container, Row, Col, Image, Button, Nav, Tab } from 'react-bootstrap'
import Title from 'components/Title'
import Chart from 'assets/images/chart.svg'
import {blueCircleArrow} from "../../../components/Icon";

function HomeFund () {

  const tabList = ['1', '2', '3', '4']
  const [eventKey, setEventKey] = useState('1')

  const showTabContent = (eventKey) => {
    setEventKey(eventKey)
  }

  return (
    <section className="home-fund">
      <Container>
        <Title variant="h2" className="d-block d-sm-none"
               data-aos="fade-up" data-aos-delay="100">
          Our Featured <br />
          Funds
        </Title>
        <Tab.Container defaultActiveKey="1">
          <Row>
            <Col xs={12} sm={5}>
              <Nav variant="pills" className="fund-number flex-sm-column">
                {
                  tabList.map((item, index) =>
                    <Nav.Item key={index} data-aos="zoom-in" data-aos-delay="100">
                      <Nav.Link eventKey={item} onClick={() => showTabContent(item)}>
                        0{item}
                      </Nav.Link>
                    </Nav.Item>
                  )
                }
              </Nav>
            </Col>
            <Col xs={12} sm={7}>
              <Tab.Content>
                <Tab.Pane eventKey={eventKey} className="active">
                  <Title variant="h2" className="d-none d-sm-block"
                         data-aos="fade-up" data-aos-delay="100">
                    Our Featured <br />
                    Funds
                  </Title>
                  <Title variant="h3" data-aos="fade-up" data-aos-delay="150">
                    Nikko AM ARK Disruptive <br />
                    Innovation Fund
                  </Title>
                  <figure className="d-none d-sm-block" data-aos="fade-up" data-aos-delay="200">
                    <Image src={Chart} alt="" />
                  </figure>
                  <div className="fund-statistic" data-aos="fade-up" data-aos-delay="250">
                    <div className="statistic-item">
                      <strong>NAV (per 100 shares)</strong>
                      <span>¥196,977</span>
                    </div>
                    <div className="statistic-item">
                      <strong>Net Assets</strong>
                      <span>7,309,193 million</span>
                    </div>
                    <div className="statistic-item">
                      <strong>Day Change</strong>
                      <span>- ¥1,094</span>
                    </div>
                    <div className="statistic-item">
                      <strong>Issued</strong>
                      <span>3,710,686,634 shares</span>
                    </div>
                  </div>
                  <figure className="d-block d-sm-none" data-aos="fade-up" data-aos-delay="200">
                    <Image src={Chart} alt="" />
                  </figure>
                  <Button variant="link" href="https://www.google.com/" target="_blank"
                          data-aos="fade-up" data-aos-delay="300">
                    <span className="me-2">Explore more funds</span> {blueCircleArrow}
                  </Button>
                </Tab.Pane>
              </Tab.Content>
            </Col>
          </Row>
        </Tab.Container>
      </Container>
    </section>
  )
}

export default HomeFund
