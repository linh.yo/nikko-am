import React, { useEffect } from 'react'
import { Container, Row, Col, Image, Button } from 'react-bootstrap'
import Shape from 'assets/images/shape/1.svg'
import MobileShape from 'assets/images/shape/4.svg'
import Title from 'components/Title'
import { blueCircleArrow } from 'components/Icon'
import { Tween, ScrollTrigger  } from 'react-gsap'

function HomeIntro () {

  return (
    <section className="home-intro">
      <ScrollTrigger start="left top" end="left 1500px" scrub={1}>
        <Tween
          to={{
            y: '1500px',
          }}
        >
          <figure className="intro-image d-none d-sm-block">
            <Image src={Shape} alt="" />
          </figure>
          <figure className="intro-image d-block d-sm-none">
            <Image src={MobileShape} alt="" />
          </figure>
        </Tween>
      </ScrollTrigger>
      <Container>
        <Row>
          <Col xs={{ span: 12, offset: 0 }} sm={{ span: 7, offset: 5 }}>
            <div className="intro-text">
              <Title variant="h2" data-aos="fade-left" data-aos-delay="100">
                Progressive Solutions <br />
                Competitive Performance <br />
                Global Citizen with <br className="d-block d-sm-none" /> Asian DNA
              </Title>
              <div className="intro-block">
                <Title variant="h3" data-aos="fade-up" data-aos-delay="150">
                  We’re one of Asia’s largest asset managers.
                </Title>
                <Button variant="link" href="https://www.google.com/" target="_blank"
                        data-aos="fade-up" data-aos-delay="200">
                  <span className="me-2">Learn more about who we are</span> {blueCircleArrow}
                </Button>
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </section>
  )
}

export default HomeIntro
