import React from 'react'
import { Container, Row, Col } from 'react-bootstrap'
import Title from 'components/Title'
import { grayCircleArrow } from 'components/Icon'

function HomeInvest () {

  const investList = [
    {
      id: 1,
      name: 'Equity',
      link: 'https://www.google.com/'
    },
    {
      id: 2,
      name: 'Bond',
      link: 'https://www.google.com/'
    },
    {
      id: 3,
      name: 'Multi-Assets',
      link: 'https://www.google.com/'
    },
    {
      id: 4,
      name: 'ETFs',
      link: 'https://www.google.com/'
    }
  ]

  return (
    <section className="home-invest">
      <Container>
        <Row>
          <Col xs={12} sm={5}>
            <Title variant="h2" data-aos="fade-right" data-aos-delay="100">
              Invest with <br />
              <span>Nikko AM</span>
            </Title>
          </Col>
          <Col xs={12} sm={7}>
            <ul className="invest-list">
              {
                investList.map((item, index) =>
                  <li key={index} data-aos="fade-left" data-aos-delay="100">
                    <a href={item.link} target="_blank">
                      <span>{item.name}</span>
                      {grayCircleArrow}
                    </a>
                  </li>
                )
              }
            </ul>
          </Col>
        </Row>
      </Container>
    </section>
  )
}

export default HomeInvest
