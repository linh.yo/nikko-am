import React from 'react'
import { Container, Row, Col, Image, Button } from 'react-bootstrap'
import Shape from 'assets/images/shape/2.svg'
import Title from 'components/Title'
import { whiteCircleArrow } from 'components/Icon'
import Text from 'components/Text'
import { Tween, ScrollTrigger  } from 'react-gsap'

function HomeInsight () {

  return (
    <section className="home-insight">
      <Container>
        <Row>
          <Col xs={12} sm={5}>
            <figure className="insight-image" data-aos="fade-right" data-aos-delay="100">
              <Image src={Shape} alt="" />
            </figure>
          </Col>
          <Col xs={12} sm={7}>
            <div className="insight-text">
              <Title variant="h4" data-aos="fade-left" data-aos-delay="100">
                Team of unique combination
              </Title>
              <Title variant="h2" data-aos="fade-left" data-aos-delay="150">
                Global Intelligence <br />
                Asian Insights <br />
                Local Experience
              </Title>
              <div className="insight-block">
                <Text data-aos="fade-up" data-aos-delay="200">
                  We transform intelligent insights into innovative, relevant investment opportunities for our clients.
                  Leveraging our unique combination of a global perspective complemented by our Asian DNA, we aspire to
                  create sophisticated and diverse solutions that set new standards in the asset management industry.
                </Text>
                <Button variant="link" href="https://www.google.com/" target="_blank"
                        data-aos="fade-up" data-aos-delay="250">
                  <span className="me-2">Learn more about what makes us unique</span> {whiteCircleArrow}
                </Button>
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </section>
  )
}

export default HomeInsight
