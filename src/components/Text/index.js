import React from 'react'

const Text = (props) => {

  const renderText = () => {
    switch (props.variant) {
      case 'p':
        return <p {...props}>{props.children}</p>
      case 'span':
        return <span {...props}>{props.children}</span>
      default:
        return <p {...props}>{props.children}</p>
    }
  }

  return renderText()
}

export default Text
