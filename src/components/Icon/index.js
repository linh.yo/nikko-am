import React from 'react'

export const blueCircleArrow = (
  <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
    <circle cx="9" cy="9" r="8" stroke="#00A4B8"/>
    <path d="M8 5.6001L11.5 9.04076L8 12.6001" stroke="#00A4B8"/>
  </svg>
)

export const whiteCircleArrow = (
  <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
    <circle cx="9" cy="9" r="8" stroke="#FAFAFA"/>
    <path d="M8 5.6001L11.5 9.04076L8 12.6001" stroke="#FAFAFA"/>
  </svg>
)

export const grayCircleArrow = (
  <svg width="32" height="32" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
    <circle cx="9" cy="9" r="8" stroke="#474747"/>
    <path d="M8 5.6001L11.5 9.04076L8 12.6001" stroke="#474747"/>
  </svg>
)
