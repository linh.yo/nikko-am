import React, { useEffect } from 'react'
import './App.css'
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import { PATHS } from './config/constants/paths'
import HomePage from './pages/HomePage'
import MainLayout from './layouts/main'
import AOS from 'aos'
import 'aos/dist/aos.css'

function App() {
  useEffect(() => {
    AOS.init({disable: 'mobile'})
    AOS.refresh()
  }, [])

  return (
    <Router>
      <MainLayout>
        <Routes>
          <Route path={PATHS.HOME_PAGE} element={<HomePage />} />
        </Routes>
      </MainLayout>
    </Router>
  )
}

export default App
