import React from 'react'
import Header from 'layouts/header'
// import Footer from 'layouts/footer'

const MainLayout = ({ children }) => {

  return (
    <React.Fragment>
      <Header />
      <main>
        {children}
      </main>
      {/*<Footer />*/}
    </React.Fragment>
  )
}

export default MainLayout
