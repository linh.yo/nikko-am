import React, { useEffect, useState } from 'react'
import { Nav, Navbar, Collapse, NavbarToggler, NavbarBrand, NavItem } from 'reactstrap'
import Logo from 'assets/images/logo.svg'
import { PATHS } from 'config/constants/paths'
import { NavLink } from 'react-router-dom'
import './index.scss'

const Header = () => {
  const [collapsed, setCollapsed] = useState(true)
  const toggleNavbar = () => setCollapsed(!collapsed)
  const [isScroll, setScroll] = useState(false)

  useEffect(() => {
    const scrollCallBack = () => {
      setScroll(window.pageYOffset > 100);
    }
    window.addEventListener('scroll', scrollCallBack);
    return () => {
      window.removeEventListener('scroll', () => scrollCallBack);
    }
  }, [])

  const navList = [
    {
      name: 'About us',
      path: PATHS.ABOUT_PAGE
    },
    {
      name: 'Sustainability',
      path: PATHS.SUSTAINABILITY_PAGE
    },
    {
      name: 'Insights & News',
      path: PATHS.INSIGHT_PAGE
    },
    {
      name: 'Strategies',
      path: PATHS.STRATEGY_PAGE
    },
    {
      name: 'Funds',
      path: PATHS.FUND_PAGE
    },
    {
      name: 'How to Invest',
      path: PATHS.INVEST_PAGE
    }
  ]

  return (
    <header id="header">
      <Navbar container
              expand="md"
              fixed="top"
              light
              className={`${isScroll ? 'scroll' : ''}`}>
        <NavbarBrand href={PATHS.HOME_PAGE}>
          <img src={Logo} alt="AYP" />
        </NavbarBrand>
        <NavbarToggler onClick={toggleNavbar} />
        <Collapse isOpen={!collapsed} navbar>
          <Nav navbar>
            {
              navList?.map((item, index) => (
                <NavItem key={index}>
                  <NavLink to={item.path} className={({ isActive }) =>
                    isActive ? 'nav-link active' : 'nav-link'
                  }>
                    {item.name}
                  </NavLink>
                </NavItem>
              ))
            }
          </Nav>
        </Collapse>
      </Navbar>
    </header>
  );
};

export default Header;
