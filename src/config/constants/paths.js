export const BASE_PATH = "/"

export const PATHS = {
  HOME_PAGE: BASE_PATH,
  ABOUT_PAGE: BASE_PATH + 'about',
  SUSTAINABILITY_PAGE: BASE_PATH + 'sustainability',
  INSIGHT_PAGE: BASE_PATH + 'insight',
  STRATEGY_PAGE: BASE_PATH + 'strategy',
  FUND_PAGE: BASE_PATH + 'fund',
  INVEST_PAGE: BASE_PATH + 'invest',
}
